import psycopg2
from flask import Flask

app = Flask(__name__)

# Your Flask app routes and classes here...

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=30080, debug=True)

# Replace with your database connection details
db_connection_config = {
    'host': 'your_db_host',
    'database': 'your_db_name',
    'user': 'your_db_user',
    'password': 'your_db_password'
}

# SQL script to create tables and insert data
sql_script = """
-- Create the Movies table
CREATE TABLE Movies (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  creator VARCHAR(255),
  type VARCHAR(255),
  imdb_rank DECIMAL(3, 1),
  release_date DATE,
  genre VARCHAR(255)
);

-- Other table creations and data inserts...

-- Insert data into the UserStars table
INSERT INTO UserStars (user_id, movie_id, datetime)
VALUES (1, 1, '2023-07-19 10:30:00');
"""

try:
    # Establish a connection to the PostgreSQL database
    conn = psycopg2.connect(**db_connection_config)
    
    # Create a cursor to interact with the database
    cursor = conn.cursor()
    
    # Execute the SQL script
    cursor.execute(sql_script)
    
    # Commit the changes
    conn.commit()
    
    print("SQL script executed successfully.")
except Exception as e:
    print(f"Error: {e}")
finally:
    # Close the cursor and connection
    cursor.close()
    conn.close()
