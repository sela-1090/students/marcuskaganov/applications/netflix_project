# Use an official Python runtime as the base image
FROM python:3.11.4

# Set the working directory in the container
WORKDIR /app

# Copy the necessary files to the container
COPY netflix-application/* /app/netflix-application/
COPY *.py /app/
COPY tests/* /app/tests/

# Install the required dependencies
RUN pip install -r requirements.txt

# Expose port 8082 
EXPOSE 8082

# Start your application 
